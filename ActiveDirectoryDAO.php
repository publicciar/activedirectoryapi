<?php

namespace ciartec;

use yii\base\Component;

class ActiveDirectoryDAO extends Component{

  public $domain;
  public $token;

  public function login($username,$password){
    $params = ["username" => $username,"password" => $password];
    return $this->request("login",'POST',$params);
  }

  public function getUsersInGroup($group){
    $params = ["group" => $group];
    return $this->request("getusersingroup",'GET',$params);
  }

  public function getUserByUsername($username){
    $params = ["username" => $username];
    return $this->request("getuserbyusername",'GET',$params);
  }

  public function existsUserInGroup($username,$group){
    $params = ["username" => $username,"group" => $group];
    return $this->request("existsuseringroup",'GET',$params);
  }

  public function getUserGroups($username){
    $params = ["username" => $username];
    return $this->request("getusergroups",'GET',$params);
  }

  public function request($action,$httpMethod,$params){
    $url = $this->domain."?r=site/".$action.'&access-token='.$this->token;
    $httpParams = http_build_query($params);
    $options = ['http' =>
      [
          'header'  => 'Content-type: application/x-www-form-urlencoded',
      ]
    ];
    switch($httpMethod){
      case 'GET':
        $url .= '&'.$httpParams;
        $options['http']['method'] = 'GET';
        break;
      case 'POST':
        $options['http']['method'] = 'POST';
        $options['http']['content'] = $httpParams;
        break;
    }

    $context  = stream_context_create($options);
    $response = file_get_contents($url,false,$context);
    return json_decode($response,true);
  }

}

?>
