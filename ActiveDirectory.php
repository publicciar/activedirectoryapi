<?php

namespace ciartec;

use yii;
use yii\base\Component;
use Exception;


class ActiveDirectory extends Component{

  public $domain;
  public $token;
  public $sessionIndex;

  public function cacheUsers(){
    $params = [];
    $users = $this->request("getusers",'GET',$params);
    $this->setUsers($users);
  }

  public function login($username,$password){
    $params = ["username" => $username,"password" => $password];
    return $this->request("login",'POST',$params);
  }

  public function request($action,$httpMethod,$params){
    $url = $this->domain."?r=site/".$action.'&access-token='.$this->token;
    $httpParams = http_build_query($params);
    $options = ['http' =>
      [
          'header'  => 'Content-type: application/x-www-form-urlencoded',
      ]
    ];
    switch($httpMethod){
      case 'GET':
        $url .= '&'.$httpParams;
        $options['http']['method'] = 'GET';
        break;
      case 'POST':
        $options['http']['method'] = 'POST';
        $options['http']['content'] = $httpParams;
        break;
    }

    $context  = stream_context_create($options);
    $response = file_get_contents($url,false,$context);
    return json_decode($response,true);
  }


  public function getUsers(){
    $users = Yii::$app->session->get($this->sessionIndex);
    return json_decode($users,true);
  }

  public function setUsers($users){
    $users = json_encode($users);
    Yii::$app->session->set($this->sessionIndex,$users);
  }

  public function getUsersInGroup($group){
    $users = $this->getUsers();
    $result = [];
    foreach($users as $user){
      if(in_array($group,$user['aGroups'])){
        $result[] = $user;
      }
    }
    return $result;
  }

  public function getUserByUsername($username){
    $users = $this->getUsers();
    foreach($users as $user){
      if($user['sNombreUsuario'] === $username){
        $result = $user;
      }
    }
    $result = isset($result)?$result:null;
    return $result;
  }

  public function existsUserInGroup($username,$group){
    $user = $this->getUserByUsername($username);
    return isset($user)?in_array($group,$user['aGroups']):false;
  }


}

?>
