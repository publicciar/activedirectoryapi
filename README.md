# Intrucciones de uso:

Agregar la siguiente dependencia a composer.json
```php
      [
        "require" => [
           "ciartec/activedirectorydao" : "dev-master"
        ]
      ]
```
Actualizar dependencias de composer.
Configurar el componente en *backend/config/main.php* (versión avanzada) o *config/web.php* (versión básica). Agregar la siguiente configuración:

```php
     [
       "components" =>
       [
            'activeDirectory' => [
                  'class' => 'ciartec\ActiveDirectory',
                  'domain' => 'http://192.168.0.227/AD/',
                  'token' => '1HZgua8UiEgfElnxW0OJkynHiEH8R0U5'
                  'sessionIndex' => 'ADUsers'
             ]
       ]
    ]
```
Utilizarlo de la siguiente manera:
```php
      $users = Yii::$app->activeDirectory->getUsersInGroup('SO-EMPLEADOS'); //buscar todos los empleados dentro del grupo 'SO-EMPLEADOS'
```

# Metodos disponibles

-------------------------------------------------------------------------------------------------------

**cacheUsers()**

```
Obtiene una colección de todos los usuarios del dominio configurado y los cachea en [$sessionIndex]
```

-------------------------------------------------------------------------------------------------------

**getUsersInGroup($group)**

```
Retorna una colección de usuarios pertenecientes a un grupo
$group => Nombre del grupo donde buscar
```

--------------------------------------------------------------------------------------------------------

**getUserByUsername($username)**

```
Retorna información de un usuario a partir de su nombre de usuario de AD
$username => Nombre de usuario del usuario de AD
```

--------------------------------------------------------------------------------------------------------

**existsUserInGroup($username,$group)**

```
Determina si un usuario pertenece a un grupo o no. Retorna true en caso afirmativo, false caso contrario
$username => Nombre de usuario del usuario de AD
$group => Nombre del grupo
```

--------------------------------------------------------------------------------------------------------

**getUserGroups($username)** 

```
Retorna una colección de todos los grupos al que pertenece un usuario
$username => Nombre de usuario del usuario de AD  
```

--------------------------------------------------------------------------------------------------------

**login($username,$password)**
```
Determina si las credenciales de un usuario son correctas. Retorna true en caso afirmativo, false caso contrario
$username => Nombre de usuario del usuario de AD  
$password => Contraseña del usuario
```